const fetch = require("node-fetch");
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

/**
 * Implement the loadPosts() function that returns a Promise with posts
 * loaded from https://jsonplaceholder.typicode.com/posts and alphabetically sorted by title
 *
 *
 * Usage example:
 * ```
 * loadPosts().then(posts => {
 *      console.log(posts); // [{ title: ... } ...]
 * });
 * ```
 */

function loadPosts() {
   try {
      return fetch("https://jsonplaceholder.typicode.com/posts")
         .then((res) => res.json())
         .then((posts) =>
            posts.sort((a, b) => {
               const first = a.title.toLowerCase();
               const second = b.title.toLowerCase();
               if (first < second) {
                  return -1;
               } else if (first > second) {
                  return 1;
               } else return 0;
            })
         );
   } catch (e) {
      console.error("Error occured. Please try again. Details:", e);
   }
}

module.exports = loadPosts;
