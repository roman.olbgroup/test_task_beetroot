/**
 * You are working in a casino and are tasked with developing a classic slot game machine.
 *
 * Considering a slot machine with three rows and three vertical reels defined like this:
 * Reel1: ['cherry', 'lemon', 'apple',  'lemon', 'banana', 'banana', 'lemon', 'lemon']
 * Reel2: ['lemon', 'apple',  'lemon', 'lemon', 'cherry', 'apple', 'banana', 'lemon']
 * Reel3: ['lemon', 'apple',  'lemon', 'apple', 'cherry', 'lemon', 'banana', 'lemon']
 *
 * Using these reels, complete the calculateResult function so that, when it's called, it gives back the result of a spin.
 * The calculateResult function takes 3 arguments, each specifying a stopping position for the corresponding reel.
 * The stopping positions describe the array index of the first symbol shown in each column. Since the machine shows
 * three rows of each reel, given a stopping position i, the reel will show symbols i, i+1, and i+2.
 *
 * 3 Cherries in a row: won 50 coins
 * 2 Cherries in a row: won 40 coins
 * 3 Apples in a row: won 20 coins
 * 2 Apples in a row: won 10 coins
 * 3 Bananas in a row: won 15 coins
 * 2 Bananas in a row: won 5 coins
 * 3 Lemons in a row: won 3 coins
 *
 *
 *   Example:
 *    Given the stopping positions (0, 2, 7), the slot machine would look like this:
 *
 *       Reel1      Reel2     Reel3
 *    --------------------------------
 *    |  cherry  |  lemon  |  lemon  |
 *    |  lemon   |  lemon  |  lemon  |
 *    |  apple   |  cherry |  apple  |
 *    --------------------------------
 *
 *   The machine shows three lemons in the second row (gives 3 coins) and two apples in the third row (gives 10 coins).
 *   Therefore the total win amount is 13.
 *
 */

function calculateResult(position1, position2, position3) {
   const positions = [position1, position2, position3];
   const reels = [
      [
         "cherry",
         "lemon",
         "apple",
         "lemon",
         "banana",
         "banana",
         "lemon",
         "lemon",
      ],
      [
         "lemon",
         "apple",
         "lemon",
         "lemon",
         "cherry",
         "apple",
         "banana",
         "lemon",
      ],
      [
         "lemon",
         "apple",
         "lemon",
         "apple",
         "cherry",
         "lemon",
         "banana",
         "lemon",
      ],
   ];
   const reelSize = reels[0].length;
   // names 'double' and 'triple' are used for better readability, I can use any other conventional names for these cases
   const results = {
      cherry: {
         double: 40,
         triple: 50,
      },
      apple: {
         double: 10,
         triple: 20,
      },
      banana: {
         double: 5,
         triple: 15,
      },
      lemon: {
         double: 0,
         triple: 3,
      },
   };

   const spinResult = positions.map((pos, idx) => [
      reels[idx][pos],
      reels[idx][(pos + 1) % reelSize],
      reels[idx][(pos + 2) % reelSize],
   ]);

   let total = 0;
   for (let i = 0; i < reels.length; i++) {
      const row = [spinResult[0][i], spinResult[1][i], spinResult[2][i]];
      if (row[1] === row[0] && row[1] === row[2]) {
         total += results[row[1]]["triple"];
         continue;
      }
      if (row[1] === row[0] || row[1] === row[2]) {
         total += results[row[1]]["double"];
      }
      if (row[0] === row[2]) {
         total += results[row[0]]["double"];
      }
   }
   return total;
}

module.exports = calculateResult;
